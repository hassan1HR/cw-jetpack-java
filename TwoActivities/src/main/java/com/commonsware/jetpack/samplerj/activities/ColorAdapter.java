/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.activities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

class ColorAdapter extends ListAdapter<Integer, ColorViewHolder> {
  private final LayoutInflater inflater;

  ColorAdapter(LayoutInflater inflater) {
    super(DIFF_CALLBACK);
    this.inflater = inflater;
  }

  @NonNull
  @Override
  public ColorViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                            int viewType) {
    View row = inflater.inflate(R.layout.row, parent, false);

    return new ColorViewHolder(row);
  }

  @Override
  public void onBindViewHolder(@NonNull ColorViewHolder holder, int position) {
    holder.bindTo(getItem(position));
  }

  private static final DiffUtil.ItemCallback<Integer> DIFF_CALLBACK =
    new DiffUtil.ItemCallback<Integer>() {
      @Override
      public boolean areItemsTheSame(@NonNull Integer oldColor, @NonNull Integer newColor) {
        return oldColor.equals(newColor);
      }

      @Override
      public boolean areContentsTheSame(@NonNull Integer oldColor, @NonNull Integer newColor) {
        return areItemsTheSame(oldColor, newColor);
      }
    };
}