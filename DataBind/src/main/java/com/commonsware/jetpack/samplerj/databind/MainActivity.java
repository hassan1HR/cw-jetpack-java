/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.databind;

import android.os.Bundle;
import com.commonsware.jetpack.samplerj.databind.databinding.ActivityMainBinding;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

public class MainActivity extends AppCompatActivity {
  private EventAdapter adapter;
  private EventViewModel vm;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    ActivityMainBinding binding =
      DataBindingUtil.setContentView(this, R.layout.activity_main);

    vm = ViewModelProviders
      .of(this, new EventViewModelFactory(savedInstanceState))
      .get(EventViewModel.class);
    adapter = new EventAdapter(getLayoutInflater(), vm.startTime);
    addEvent("onCreate()");

    binding.items.setLayoutManager(new LinearLayoutManager(this));
    binding.items.addItemDecoration(
      new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
    binding.items.setAdapter(adapter);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    vm.onSaveInstanceState(outState);
  }

  @Override
  protected void onStart() {
    super.onStart();

    addEvent("onStart()");
  }

  @Override
  protected void onResume() {
    super.onResume();

    addEvent("onResume()");
  }

  @Override
  protected void onPause() {
    addEvent("onPause()");

    super.onPause();
  }

  @Override
  protected void onStop() {
    addEvent("onStop()");

    super.onStop();
  }

  @Override
  protected void onDestroy() {
    addEvent("onDestroy()");

    super.onDestroy();
  }

  private void addEvent(String message) {
    vm.addEvent(message, hashCode());
    adapter.submitList(vm.events);
  }
}
