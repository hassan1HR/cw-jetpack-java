/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.style;

import android.os.Bundle;
import android.os.SystemClock;
import java.util.ArrayList;
import androidx.lifecycle.ViewModel;

public class EventViewModel extends ViewModel {
  private static final String STATE_EVENTS = "events";
  private static final String STATE_START_TIME = "startTime";

  final ArrayList<Event> events;
  final long startTime;

  EventViewModel(Bundle state) {
    if (state != null) {
      events = state.getParcelableArrayList(STATE_EVENTS);
      startTime = state.getLong(STATE_START_TIME);
    }
    else {
      events = new ArrayList<>();
      startTime = SystemClock.elapsedRealtime();
    }
  }

  void addEvent(String message, int activityHash) {
    events.add(new Event(message, activityHash, hashCode()));
  }

  void onSaveInstanceState(Bundle state) {
    state.putParcelableArrayList(STATE_EVENTS, events);
    state.putLong(STATE_START_TIME, startTime);
  }

  @Override
  protected void onCleared() {
    events.clear();
  }
}
